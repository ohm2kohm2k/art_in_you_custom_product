var colorPickerOpened = false;

$('.dropdown').outclick(function(event) {
  if (!colorPickerOpened) {
    $(this).hide()
  }
});

function openDropdown() {
  $(this).find('.dropdown').show();
}

$("#tool-color").click(openDropdown);
$("#tool-img").click(openDropdown);
$("#tool-shape").click(openDropdown);
$("#tool-text").click(openDropdown);


$('#colorSelector').ColorPicker({
  color: '#0000ff',
  onShow: function (colpkr) {
    colorPickerOpened = true;
    $(colpkr).show();
    return false;
  },
  onHide: function (colpkr) {
    colorPickerOpened = false;
    $(colpkr).hide();
    return false;
  },
  onChange: function (hsb, hex, rgb) {
    $('#colorSelector > .diy-color-box').css('backgroundColor', '#' + hex);
  }
});

$('#colorSelectorText').ColorPicker({
  color: '#0000ff',
  onShow: function (colpkr) {
    colorPickerOpened = true;
    $(colpkr).show();
    return false;
  },
  onHide: function (colpkr) {
    colorPickerOpened = false;
    $(colpkr).hide();
    return false;
  },
  onChange: function (hsb, hex, rgb) {
    $('#colorSelectorText > .diy-color-box').css('backgroundColor', '#' + hex);
  }
});
var canvas_2d;
var loading_first_time = false;

function ini_canvas_2d() {
  canvas_2d = new fabric.Canvas('canvas-2d');
  var canvas_size = $('.custom-box').width();
  canvas_2d.backgroundColor = '#eee';
  canvas_2d.setBackgroundImage(req_model.pathPicPattern,
    canvas_2d.renderAll.bind(canvas_2d), {
      backgroundImageOpacity: 0.5,
      width: canvas_size,
      height: canvas_size,
    }
  );
  canvas_2d.on('after:render', changeTexture);
}




function addImageFirstTime() {
  for (var i = 0; i < req_model.imgUploaded.length; i++) {

    insertImgWithUrl(req_model.imgUploaded[i]);

  }
}

function insertImgWithUrl(url) {

  fabric.Image.fromURL(url, function(img) {
    img.set({
      scaleX: 0.5,
      scaleY: 0.5,
      left: canvas_2d.width/3,
      top: canvas_2d.height/3
    })
    canvas_2d.add(img);
  }, {crossOrigin: 'Anonymous'});

}


// var imageLoader = document.getElementById('imageLoader');
// imageLoader.addEventListener('change', handleImage, false);

// function handleImage(e) {
//   var reader = new FileReader();
//   reader.onload = function(event) {
//     var img = new Image();
//     img.onload = function() {
//       var imgInstance = new fabric.Image(img, {
//         scaleX: 0.1,
//         scaleY: 0.1
//       })
//       canvas_2d.add(imgInstance);
//     }
//     img.src = event.target.result;
//   }
//   reader.readAsDataURL(e.target.files[0]);
// }




window.deleteObject = function() {
  if (canvas_2d.getActiveObject()) {
    canvas_2d.getActiveObject().remove();
  } else {
    $('#alert-select-img').click();
  }
}

function onWindowResize() {
  var canvas_size = $('.custom-box').width();
  canvas_2d.setHeight(canvas_size);
  canvas_2d.setWidth(canvas_size);
  canvas_2d.setBackgroundImage(req_model.pathPicPattern,
    canvas_2d.renderAll.bind(canvas_2d), {
      backgroundImageOpacity: 0.5,
      width: canvas_size,
      height: canvas_size,
    }
  );

  renderer.setSize( canvas_size, canvas_size );
}


var timeoutInCase = {};

function changeTexture() {
  if (!loading_first_time) {
    addImageFirstTime();
    loading_first_time = true;
  }
  if (model3D_texture && loading_first_time && $('.loading-modal').is(':visible')) {
    $('.loading-modal').hide();
    onWindowResize();
    window.addEventListener( 'resize', onWindowResize, false );
  }
  if (model3D_texture) {

    if (window.innerWidth < 500) {
      debounce(function() {
        model3D_texture.material.materials[indexBody].map.needsUpdate = true;
      }, 1000, false, 'change_texture_time');
    } else {
      model3D_texture.material.materials[indexBody].map.needsUpdate = true;
    }
  }
}


function debounce(func, wait, immediate, key) {
  var context = this;
  var args = arguments;
  var callNow = immediate && !timeoutInCase[key];
  clearTimeout(timeoutInCase[key]);
  timeoutInCase[key] = setTimeout(function() {
    timeoutInCase[key] = null;
    if (!immediate) {
      func.apply(context, args);
    }
  }, wait);
  if (callNow) func.apply(context, args);
}




function handleChangeBG(color) {
  canvas_2d.backgroundColor = color;
  model3D_texture.material.materials[indexCloth].color = new THREE.Color(color);
  canvas_2d.renderAll();
}





function insert_shape() {
  var color = $('#colorSelector > .diy-color-box').css('backgroundColor');
  var object_to_insert;
  var size = canvas_2d.height/4;

  switch($('#select_shape').val()) {
    case 'cicle':
      object_to_insert = new fabric.Circle({radius: size/2, fill: color, left: canvas_2d.width/3, top: canvas_2d.height/3});
      break;
    case 'triangle':
      object_to_insert = new fabric.Triangle({width: size, height: size, fill: color, left: canvas_2d.width/3, top: canvas_2d.height/3});
      break
    case 'line':
      object_to_insert = new fabric.Rect({width: size*2, height: 10, fill: color, left: canvas_2d.width/4, top: canvas_2d.height/2});
      break
    case 'square':
      object_to_insert = new fabric.Rect({width: size, height: size, fill: color, left: canvas_2d.width/3, top: canvas_2d.height/3});
      break
  }

  canvas_2d.add(object_to_insert);
}

function insert_text() {
  var color = $('#colorSelectorText > .diy-color-box').css('backgroundColor');
  var text = $('#input_text_insert').val();
  var font_family = $('#select_text_insert').val();
  var size = 40;
  var text = new fabric.Text(text, { fill: color, fontFamily: font_family, fontSize: size, left: canvas_2d.width/4, top: canvas_2d.height/2});
  canvas_2d.add(text);
}


function export2dToSvg() {
  var svg = canvas_2d.toSVG();
  return new File([svg], 'temp-img-2d.svg', {type: "image/svg+xml;charset=utf-8"});
}

fabric.Image.prototype.getSvgSrc = function() {
  return this.toDataURLforSVG();
};

fabric.Image.prototype.toDataURLforSVG = function(options) {
  var el = fabric.util.createCanvasElement();
  el.width = this._element.naturalWidth;
  el.height = this._element.naturalHeight;
  el.getContext("2d").drawImage(this._element, 0, 0);
  var data = el.toDataURL(options);
  return data;
};

function export_2d_json() {
  return JSON.stringify(canvas_2d.toJSON());
}

function clone_obj() {
  var object = fabric.util.object.clone(canvas_2d.getActiveObject());
   canvas_2d.deactivateAll().renderAll();
   object.set("top", object.top+15);
   object.set("left", object.left+15);
   canvas_2d.add(object);
}

function bring_front() {
  canvas_2d.bringToFront(canvas_2d.getActiveObject());
  canvas_2d.deactivateAll().renderAll();
}

function send_back() {
  canvas_2d.sendToBack(canvas_2d.getActiveObject());
  canvas_2d.deactivateAll().renderAll();
}


document.onkeydown = function(event) {
  var key;
   if(window.event){
       key = window.event.keyCode;
   }
   else{
       key = event.keyCode;
   }

   console.log(key);
   switch(key){
       case 67: // Ctrl+C
        clone_obj()
           break;
       case 46: // delete
        canvas_2d.getActiveObject().remove();
       default:
           break;
   }
}

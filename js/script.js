//@prepros-append 2d_control.js
//@prepros-append nav_custom_tool.js

var container;

var texture;
var model3D_texture;
var model3D;

var indexBody, indexDummy, indexCloth;

var camera, scene, renderer;

var mouseX = 0, mouseY = 0;


      function init() {

        container = document.getElementById('canvas-3d');

        camera = new THREE.PerspectiveCamera( 45, 1, 1, 2000 );
        camera.position.z = 65;

        // scene

        scene = new THREE.Scene();
        scene.background = new THREE.Color( '#eee' );

        var ambient = new THREE.AmbientLight( 0xffffff, 0.3 );
        scene.add( ambient );

        var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.2 );
        directionalLight.position.set( 50, 100, 50 );
        scene.add( directionalLight );

        var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.2 );
        directionalLight.position.set( -50, 100, 50 );
        scene.add( directionalLight );

        var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.1 );
        directionalLight.position.set( 50, -80, 50 );
        scene.add( directionalLight );

        var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.1 );
        directionalLight.position.set( -50, -80, 50 );
        scene.add( directionalLight );

        var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.15 );
        directionalLight.position.set( -150, 100, -250 );
        scene.add( directionalLight );

        var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.15 );
        directionalLight.position.set( 150, 0, -250 );
        scene.add( directionalLight );

        var hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.3 );
        hemiLight.color.setHSL( 255, 255 , 255 );
        hemiLight.groundColor.setHSL( 255, 255, 255 );
        hemiLight.position.set( 0, 1000, 0 );
        scene.add( hemiLight );

        var hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.1 );
        hemiLight.color.setHSL( 0.6, 1, 0.6 );
        hemiLight.groundColor.setHSL( 0.095, 1, 0.75 );
        hemiLight.position.set( 0, 1500, 0 );
        scene.add( hemiLight );


        var manager = new THREE.LoadingManager();
        manager.onProgress = function ( item, loaded, total ) {
          // console.log( item, loaded, total );
        };

        var onProgress = function ( xhr ) {
          if ( xhr.lengthComputable ) {
            var percentComplete = xhr.loaded / xhr.total * 100;
            // console.log( Math.round(percentComplete, 2) + '% downloaded' );
          }
        };

        var onError = function ( xhr ) {
        };


        // model

        loader = new THREE.JSONLoader();
        loader.crossorigin = true;
        loader.load( req_model.pathModel, function ( geometry, materials ) {

          indexBody = materials.map(function(e) { return e.name; }).indexOf('cloth_body_in');
          indexDummy = materials.map(function(e) { return e.name; }).indexOf('dummy_body_in');
          indexCloth = materials.map(function(e) { return e.name; }).indexOf('cloth_side_in');

          // canvas to design
          materials[indexBody] = new THREE.MeshStandardMaterial( {
            map: new THREE.Texture(document.getElementById('canvas-2d')),
            roughnessMap: null,
            color: 0xffffff,
            metalness: 0.02,
            roughness: 0.45,
            shading: THREE.SmoothShading
          } );

          // dummy color
          materials[indexDummy] = new THREE.MeshStandardMaterial( {
            color: 0xffffff,
            metalness: 0.02,
            roughness: 0.45,
            shading: THREE.SmoothShading
          } );

          // cloth color
          materials[indexCloth] = new THREE.MeshStandardMaterial( {
            color: 0xffffff,
            metalness: 0.05,
            roughness: 0.5,
            shading: THREE.SmoothShading
          } );


          model3D_texture = new THREE.Mesh( geometry, new THREE.MeshFaceMaterial( materials ));
          model3D_texture.geometry.computeVertexNormals();
          model3D_texture.geometry.mergeVertices();
          model3D_texture.position.y = 0;
          scene.add( model3D_texture );
        }, onProgress, onError );



        try {
          renderer = new THREE.WebGLRenderer({ alpha: false, preserveDrawingBuffer: true, antialias: true});
        } catch(e) {
          if (!webglAvailable()) {
            $('.loading-modal').hide();
            setTimeout(function() {
              $('#alert-not-support-webgl').click();
            }, 300);
            return;
          }
        }
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.shadowMapEnabled = true;
        renderer.shadowMapSoft = true;
        renderer.shadowMapType = THREE.PCFShadowMap;
        renderer.setSize( 460, 460 );
        container.appendChild( renderer.domElement );

        var controls = new THREE.OrbitControls( camera, renderer.domElement );
        controls.enableDamping = true;
        controls.enableZoom = false;
        controls.minPolarAngle = 0.5;
        controls.maxPolarAngle = Math.PI/2;


      }


      function animate() {

        requestAnimationFrame( animate );
        render();

      }

      var fd_full_image = [new FormData(), new FormData(), new FormData(), new FormData(), new FormData(), new FormData()];
      var check_render_finish_full_image = 6;
      var img_url_left = '';
      var img_url_right = '';

      var callback_when_finish_upload_img;

      function render() {
        if (check_render_finish_full_image === 0) {
          camera.position.set(3.3869945730605475, 3.980102097228898e-15, 64.91169592424818);
          fd_full_image[4].append('file', export2dToSvg());
          check_render_finish_full_image = 1;
        } else if (check_render_finish_full_image === 1) {
          img_url_right = renderer.domElement.toDataURL();
          fd_full_image[0].append('file', dataURLtoFile(img_url_right));
          camera.position.set(46.03570146855141, 3.980102097228898e-15, 45.88806152256176);
          check_render_finish_full_image = 2;
        } else if (check_render_finish_full_image === 2) {
          fd_full_image[1].append('file', dataURLtoFile(renderer.domElement.toDataURL()));
          camera.position.set(0.7713603784407379, 3.980102097228898e-15, -64.99542294013149);
          check_render_finish_full_image = 3;
        } else if (check_render_finish_full_image === 3) {
          img_url_left = renderer.domElement.toDataURL();
          fd_full_image[2].append('file', dataURLtoFile(img_url_left));
          camera.position.set(-57.21292614323928, 3.980102097228898e-15, -30.849328714386107);
          check_render_finish_full_image = 4;
        } else if (check_render_finish_full_image === 4) {
          fd_full_image[3].append('file', dataURLtoFile(renderer.domElement.toDataURL()));
          check_render_finish_full_image = 5;
        } else if (check_render_finish_full_image === 5) {
          var callback = function(urlImgMerge) {
            fd_full_image[5].append('file', dataURLtoFile(urlImgMerge));
            upload_full_image();
          }
          getMergeImgUrl(callback);
          check_render_finish_full_image = 6;
        }
        camera.lookAt( scene.position );
        renderer.render( scene, camera );
      }


      function sentUploadFinishImage(callback) {
        callback_when_finish_upload_img = callback;

        $('.loading-modal').show();
        setTimeout(function () {
          renderer.setSize( 460, 460 );
          canvas_2d.deactivateAll().renderAll();
          check_render_finish_full_image = 0;
        }, 100);
      }

      function upload_full_image() {
        var array_full_image = ['','','','', '', ''];

        $.when(
          $.ajax({type: "POST", contentType: false, url: "/api/upload.php",processData: false,
            data: fd_full_image[0]}).then(function(data) {array_full_image[0] = data.result.full}),
          $.ajax({type: "POST", contentType: false, url: "/api/upload.php",processData: false,
            data: fd_full_image[1]}).then(function(data) {array_full_image[1] = data.result.full}),
          $.ajax({type: "POST", contentType: false, url: "/api/upload.php",processData: false,
            data: fd_full_image[2]}).then(function(data) {array_full_image[2] = data.result.full}),
          $.ajax({type: "POST", contentType: false, url: "/api/upload.php",processData: false,
            data: fd_full_image[3]}).then(function(data) {array_full_image[3] = data.result.full}),
          $.ajax({type: "POST", contentType: false, url: "/api/upload.php",processData: false,
            data: fd_full_image[4]}).then(function(data) {array_full_image[4] = data.result.full}),
          $.ajax({type: "POST", contentType: false, url: "/api/upload.php",processData: false,
            data: fd_full_image[5]}).then(function(data) {array_full_image[5] = data.result.full})
        ).fail(function(){
                  console.log('error');
                })
        .then( function() {
          callback_when_finish_upload_img({images: array_full_image, json2d: export_2d_json()})
          // console.log(array_full_image);
          // var tempImg = '<img src="'+ array_full_image[0] +'">';
          // tempImg += '<img src="'+ array_full_image[1] +'">';
          // tempImg += '<img src="'+ array_full_image[2] +'">';
          // tempImg += '<img src="'+ array_full_image[3] +'">';
          // tempImg += '<img src="'+ array_full_image[4] +'">';
          // tempImg += '<img src="'+ array_full_image[5] +'">';
          // document.body.innerHTML = tempImg;
        });
      }

      function getMergeImgUrl(callback) {
        var c = document.createElement("canvas");
          c.width = 920;
          c.height = 460;
          var ctx = c.getContext("2d");
          var image_left = new Image();
          image_left.src = img_url_left;
          image_left.onload = function() {
             ctx.drawImage(image_left, 0, 0, 460, 460);
             var image_right = new Image();
             image_right.src = img_url_right;
             image_right.onload = function() {
                ctx.drawImage(image_right, 460, 0, 460, 460);
                var img = c.toDataURL("image/png");
                callback(img);
             }
          };
      }

      function dataURLtoFile(dataurl) {
          var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
              bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
          while(n--){
              u8arr[n] = bstr.charCodeAt(n);
          }
          return new File([u8arr], 'temp-img.png', {type:mime});
      }

      function webglAvailable() {
          try {
              var canvas = document.createElement("canvas");
              return !!
                  window.WebGLRenderingContext &&
                  (canvas.getContext("webgl") ||
                      canvas.getContext("experimental-webgl"));
          } catch(e) {
              return false;
          }
      }
